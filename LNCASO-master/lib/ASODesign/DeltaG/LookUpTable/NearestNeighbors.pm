package ASODesign::DeltaG::LookUpTable::NearestNeighbors;

use 5.006;
use Moose; # automatically turns on strict and warnings

has 'sequence' => (is => 'rw', isa => 'Str', required => 1);
has 'delta_g' => (is => 'rw', isa => 'Num', required => 1);


1; # End of ASODesign::DeltaG::LookUpTable::NearestNeighbors
