package ASODesign::DeltaG::Calculator;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::DeltaG::LookUpTable;

# file for the lut
has 'lut_file' => ( is  => 'rw', isa => 'Str', required => 1 );
has 'lut' => ( is  => 'rw', isa => 'ASODesign::DeltaG::LookUpTable' );


sub BUILD {
	my $self = shift;

	unless (-f $self->lut_file) {
		confess("Cannot find file ".$self->lut_file);
	}

	$self->lut( ASODesign::DeltaG::LookUpTable->new(filename => $self->lut_file) );
	
}

sub calculate_delta_g {
	my $self = shift;
	my $probe = shift;

	my $delta_g = 0;

	# nearest neighbors
	foreach my $nn ($probe->neighbors) {
		$delta_g += $self->lut->nn($nn)->delta_g;
	}

	#terminal bases
	foreach my $b ($probe->terminal_bases) {
		# T and C terminal bases are undefined in lut
		$b =~ tr/TC/AG/;
		$delta_g += $self->lut->nn($b)->delta_g;
	}

	$probe->delta_g($delta_g);
	return $delta_g;
}

sub probes_delta_g {
	my $self = shift;
	my $probes = shift;

	foreach my $p (@{$probes}) {
		$self->calculate_delta_g($p) or return 0;
	}
	return 1;
}

1; # End of ASODesign::DeltaG::Calculator
