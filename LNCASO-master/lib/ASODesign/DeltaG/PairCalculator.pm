package ASODesign::DeltaG::PairCalculator;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::DeltaG::PairLookUpTable;

# file for the lut
has 'lut_file' => ( is  => 'rw', isa => 'Str', required => 1 );
has 'lut' => ( is  => 'rw', isa => 'ASODesign::DeltaG::PairLookUpTable' );


sub BUILD {
	my $self = shift;

	unless (-f $self->lut_file) {
		confess("Cannot find file ".$self->lut_file);
	}

	$self->lut( ASODesign::DeltaG::PairLookUpTable->new(filename => $self->lut_file) );
	
}

sub calculate_delta_g {
	my $self = shift;
	my $probe = shift; #ASODesign::Probe object
	my $match = shift; #ASODesign::Probe object

	my $delta_g = 0;

	my @probe_neighbors = $probe->neighbors;
	my @match_neighbors = $match->neighbors;

	confess "Probe and match differ in length" if (scalar(@probe_neighbors) != scalar(@match_neighbors));

	for (my $i = 0; $i < scalar(@probe_neighbors); $i++) {
		$delta_g += $self->lut->nn($probe_neighbors[$i], $match_neighbors[$i])->delta_g;
	}

	my @probe_terminal_bases = $probe->terminal_bases;
	#my @match_terminal_bases = $match->terminal_bases;

	for (my $i = 0; $i < scalar(@probe_terminal_bases); $i++) {
		# for simplicity, we will assume the free engery for terminal base is the same for
		# matched ws mismatched 
		my $match_terminal_base = $probe_terminal_bases[$i];
		$match_terminal_base =~ tr/ATGC/TACG/;
		$delta_g += $self->lut->nn($probe_terminal_bases[$i], $match_terminal_base)->delta_g;
	}

	#$probe->delta_g($delta_g); # NOT for mismatched probes
	return $delta_g;
}

# sub probes_delta_g {
# 	my $self = shift;
# 	my $probes = shift;

# 	foreach my $p (@{$probes}) {
# 		$self->calculate_delta_g($p) or return 0;
# 	}
# 	return 1;
# }

1; # End of ASODesign::DeltaG::PairCalculator
