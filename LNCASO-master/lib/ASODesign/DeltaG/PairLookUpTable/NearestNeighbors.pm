package ASODesign::DeltaG::PairLookUpTable::NearestNeighbors;

use 5.006;
use Moose; # automatically turns on strict and warnings
extends 'ASODesign::DeltaG::LookUpTable::NearestNeighbors';

has 'sequence_as' => (is => 'rw', isa => 'Str', required => 1);


1; # End of ASODesign::DeltaG::PairLookUpTable::NearestNeighbors
