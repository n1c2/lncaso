package ASODesign::DeltaG::LookUpTable;

use 5.006;
use Moose; # automatically turns on strict and warnings

use ASODesign::DeltaG::LookUpTable::NearestNeighbors;

has 'filename' => (is => 'ro', isa => 'Str', required => 1);
has 'auto_construct' => (is => 'rw', isa => 'Bool', default => 1);


has 'table' => (is => 'rw', isa => 'HashRef', default => sub { {} });
# { 'NN' => NearestNeighbors_object, .. }

# constructor
sub BUILD {
	my $self = shift;

	if ($self->auto_construct) {
		$self->construct;
	}
}

# construct the table from file
sub construct {
	my $self = shift;

	open (FILE, $self->filename) or confess("Can not open file ".$self->filename);
	while (<FILE>) {
		chomp;
		next if $_ =~ /^#/; # skip comment lines
		next if $_ =~ /^sequence/; # skip header
		my ($sequence, $delta_g) = split("\t");
		my $nn = ASODesign::DeltaG::LookUpTable::NearestNeighbors->new(sequence => $sequence, delta_g => $delta_g);
		$self->add_nn($nn) or confess("Error while parsing ${self->filename}, sequence '$sequence' found more than once");
	}
	close (FILE);

	return 1;
}

# add NearestNeighbors to table
sub add_nn {
	my $self = shift;
	my $nn = shift;
	my $table = $self->table;

	if ($table->{$nn->sequence}) {
		# table allready contains an element for this sequence
		return;
	}
	$table->{$nn->sequence} = $nn;
	return 1;
}

# get NearestNeighbors for sequence
sub nn {
	my $self = shift;
	my $seq = shift;

	return $self->table->{$seq};
}

1; # End of ASODesign::DeltaG::LookUpTable
