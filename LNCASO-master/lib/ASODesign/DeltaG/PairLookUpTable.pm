package ASODesign::DeltaG::PairLookUpTable;

use 5.006;
use Moose; # automatically turns on strict and warnings
extends 'ASODesign::DeltaG::LookUpTable';

use ASODesign::DeltaG::PairLookUpTable::NearestNeighbors;

# construct the table from file
sub construct {
	my $self = shift;

	open (FILE, $self->filename) or confess("Can not open file ".$self->filename);
	while (<FILE>) {
		chomp;
		next if $_ =~ /^#/; # skip comment lines
		next if $_ =~ /^sequence/; # skip header
		my ($sequence, $sequence_as, $delta_g) = split("\t");
		my $nn = ASODesign::DeltaG::PairLookUpTable::NearestNeighbors->new(	sequence => $sequence,
		                                                                    sequence_as => $sequence_as,
		                                                                    delta_g => $delta_g);
		$self->add_nn($nn) or confess("Error while parsing ".$self->filename.", combination '$sequence'-'$sequence_as' found more than once");
	}
	close (FILE);

	return 1;
}

# add NearestNeighbors to table
sub add_nn {
	my $self = shift;
	my $nn = shift;
	my $table = $self->table;

	if ($table->{$nn->sequence}) {
		# table allready contains an element for this sequence
		if ($table->{$nn->sequence}->{$nn->sequence_as}) {
			# table allready contains an element for this antisense sequence
			return;
		}
	} else {
		$table->{$nn->sequence} = {};
	}
	$table->{$nn->sequence}->{$nn->sequence_as} = $nn;
	return 1;
}

# get NearestNeighbors for sequence
sub nn {
	my $self = shift;
	my $seq = shift;
	my $seq_as = shift;

	if ($self->table->{$seq}) {
		if ($self->table->{$seq}->{$seq_as}) {
			return $self->table->{$seq}->{$seq_as};;
		}
	}
	confess "Could not find pair ".$seq."-".$seq_as;
}

1; # End of ASODesign::DeltaG::PairLookUpTable
