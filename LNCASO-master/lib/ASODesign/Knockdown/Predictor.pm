package ASODesign::Knockdown::Predictor;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::Probe;
use Statistics::R;

#has 'r_script' => (isa => 'Str', is => 'rw', default => 'score_probes.R');
has 'gam_model' => (isa => 'Str', is => 'rw', required => 1);

sub score_probes {
	my $self = shift;
	my $probes = shift;

    my $R = Statistics::R->new();

    $R->run('load("'.$self->gam_model.'")');

    foreach my $probe (@{$probes}) {
        my @R_commands = (
            sprintf("probe_data = data.frame( dimer_delta_g = %f, log_accessibility = log( %f ), delta_g = %f)", $probe->dimer_delta_g, $probe->accessibility, $probe->delta_g),
            'logknockdown = -1*predict(mod_gam_final, probe_data)'
        );
        $R->run(@R_commands);
        my $score = $R->get('logknockdown');

        $score = 0 if $score =~ /Inf/;
        $probe->score($score);
    }

    $R->stop();

    return 1;
}

sub score_probe {
	my $self = shift;
	my $probe = shift;

	return $self->score_probes([$probe]);

}

1; # End of ASODesign::Knockdown::Predictor
