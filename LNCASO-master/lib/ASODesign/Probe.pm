package ASODesign::Probe;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::Specificity::Result;

our $VERSION = '0.01';

has 'sequence' => (is => 'rw', isa => 'Str', required => 1);

has 'delta_g' => (is => 'rw', isa => 'Num', required => 0);
has 'dimer_delta_g' => (is => 'rw', isa => 'Num', required => 0);
has 'accessibility' => (is => 'rw', isa => 'Num', required => 0);

has 'chromosome' => (is => 'rw', isa => 'Str', required => 0);
has 'start' => (is => 'rw', isa => 'Int', required => 0);
has 'end' => (is => 'rw', isa => 'Int', required => 0);
has 'gene_id' => (is => 'rw', isa => 'Str', required => 0);
has 'transcript_id' => (is => 'rw', isa => 'Str', required => 0);

has 'pos' => (is => 'rw', isa => 'Int', required => 0); # position within target

has 'is_specific' => (is => 'rw', isa => 'Int', required => 0);
has 'specificity' => (is => 'rw', isa => 'ASODesign::Specificity::Result', required => 0);

has 'score' => (is => 'rw', isa => 'Num', required => 0);

sub length {
	my $self = shift;
	
	my $tmp_seq = $self->sequence;
	my $length = $tmp_seq =~ s/[ATGCU]/$&/g;

	return($length);
}

sub neighbors {
	my $self = shift;

	my @bases = $self->bases;
	my @neighbors;
	for (my $i = 0; $i < $self->length-1; $i++) {
		push(@neighbors, $bases[$i].$bases[$i+1])
	}
	

	return @neighbors;
}

sub bases {
	my $self = shift;

	my @bases = ( $self->sequence =~ /[m\+]?[ATGCU]/g );

	return @bases;
}

sub terminal_bases {
	my $self = shift;

	my @terminal_bases = ( ($self->bases)[0], ($self->bases)[-1] );

	return @terminal_bases;
}

sub raw_sequence {
	my $self = shift;
	my $sequence = $self->sequence();

	$sequence =~ s/[^ATGCU]//gi;
	$sequence =~ tr/U/T/; # RNA to DNA

	return $sequence;
}

1; # End of ASODesign::Probe
