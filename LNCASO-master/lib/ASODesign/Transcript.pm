package ASODesign::Transcript;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::Accessibility::Profile;

has 'id' => (is => 'rw', isa => 'Str', required => 0, default => 'sequence');
has 'sequence' => (is => 'rw', isa => 'Str', required => 1);

has 'chromosome' => (is => 'rw', isa => 'Str', required => 0);
has 'start' => (is => 'rw', isa => 'Int', required => 0);
has 'end' => (is => 'rw', isa => 'Int', required => 0);

has 'accessibility_profile' => (is => 'rw', isa => 'ASODesign::Accessibility::Profile', required => 0);

sub length {
	my $self = shift;
	
	my $tmp_seq = $self->sequence;
	my $length = $tmp_seq =~ s/[ATGCU]/$&/g;

	return($length);
}

1; # End of ASODesign::Probe
