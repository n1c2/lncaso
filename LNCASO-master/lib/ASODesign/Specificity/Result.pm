package ASODesign::Specificity::Result;

use Moose;
use ASODesign::Probe;
use List::MoreUtils qw(uniq);

=head1 NAME

ASODesign::Specificity::Result - The great new ASODesign::Specificity::Result!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

has 'perfect_matches' => (isa => 'ArrayRef', is => 'rw', default => sub { [] });
has 'single_mismatches' => (isa => 'ArrayRef', is => 'rw', default => sub { [] });
has 'double_mismatches' => (isa => 'ArrayRef', is => 'rw', default => sub { [] });

has 'gene_ids' => (isa => 'ArrayRef', is => 'rw', default => sub { [] });

has 'is_specific' => (isa => 'Num', is => 'rw', default => 0);

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use ASODesign::Specificity::Result;

    my $foo = ASODesign::Specificity::Result->new();
    ...

=head1 EXPORT

A list of functions that can be exported.  You can delete this section
if you don't export anything, such as for a purely object-oriented module.

=head1 SUBROUTINES/METHODS

=head2 function1

=cut

sub nr_perfect_matches() {
	my $self = shift;
	return scalar(@{$self->perfect_matches});
}

sub nr_single_mismatches() {
	my $self = shift;
	return scalar(@{$self->single_mismatches});
}

sub nr_double_mismatches() {
	my $self = shift;
	return scalar(@{$self->double_mismatches});
}

sub delta_g_perfect_match() {
	my $self = shift;
	#unless ($self->nr_perfect_matches == 1) {
	#	confess("Multiple perfect matches found");
	#}
	return $self->perfect_matches->[0]->delta_g();
}

sub nr_single_mismatches_delta_g() {
	my $self = shift;
	my $dg_threshold = shift;

	my @filtered_targets = grep { $_->delta_g <= $dg_threshold } @{$self->single_mismatches};

	if ($self->perfect_matches->[0] && $self->perfect_matches->[0]->gene_id) {
		my $gene_id = $self->perfect_matches->[0]->gene_id;
		@filtered_targets = grep { $_->gene_id ne $gene_id } @filtered_targets;
	}

	return scalar(@filtered_targets);
}

sub nr_double_mismatches_delta_g() {
	my $self = shift;
	my $dg_threshold = shift;

	my @filtered_targets = grep { $_->delta_g <= $dg_threshold } @{$self->double_mismatches};
	return scalar(@filtered_targets);
}

sub nr_geneids_perfect_matches() {
	my $self = shift;
	my @gene_ids = map { $_->gene_id ? ($_->gene_id) : () } @{$self->perfect_matches};
	@gene_ids = uniq @gene_ids;
	return scalar(@gene_ids);
}

=head1 AUTHOR

Pieter-Jan Volders, C<< <pieterjan.volders at ugent.be> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-asodesign-probe at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=ASODesign-Probe>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc ASODesign::Specificity::Result


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=ASODesign-Probe>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/ASODesign-Probe>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/ASODesign-Probe>

=item * Search CPAN

L<http://search.cpan.org/dist/ASODesign-Probe/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2014 Pieter-Jan Volders.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of ASODesign::Specificity::Result
