package ASODesign::Specificity::Checker;

use Moose;
use ASODesign::Probe;
use ASODesign::Specificity::Result;
use ASODesign::DeltaG::PairCalculator;
use File::Temp qw(tempdir);
use Cwd;

=head1 NAME

ASODesign::Specificity::Checker - The great new ASODesign::Specificity::Checker!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

has 'bowtie_cmd' => (isa => 'Str', is => 'rw', default => 'bowtie');
has 'bowtie_index' => (isa => 'Str', is => 'rw', required => 1);
has 'pair_lut_file' => (isa => 'Str', is => 'rw', required => 1);

# 'none' => do not call specificity, just calculate all off targets
# 'unique' => call specific if only 1 perfect match is found
# 'thermodynamic' => call specific if no off target hit with a delta G
# within 'max_delta_g_diff_offtarget' of the perfect match
has 'specificity_method' => (isa => 'Str', is => 'rw', default => 'thermodynamic');


# 'gdna' => take into account + and - strand mappings
# 'cdna' => take into account - strand mappings only
has 'index_type' => (isa => 'Str', is => 'rw', default => 'gdna');

has 'max_delta_g_diff_offtarget' => (isa => 'Num', is => 'rw', default => 3);

has 'debug' => (isa => 'Num', is => 'rw', default => 0);

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use ASODesign::Specificity::Checker;

    my $foo = ASODesign::Specificity::Checker->new();
    ...

=head1 SUBROUTINES/METHODS

=head2 check_probes
Input : Arrayref with ASODesign::Probe objects
Output: Arrayref of ASODesign::Specificity::Result objects

=cut

sub check_probes() {
	my $self = shift;
	my $list = shift;

	my $result_list = [];

	# Delta G calculator
	my $calc = ASODesign::DeltaG::PairCalculator->new(lut_file => $self->pair_lut_file);

	# create temp directory
	my $curr_dir = getcwd; # the current directory
	my $temp_dir;
	if ($self->debug()) {
		$temp_dir = tempdir( CLEANUP => 0 );
		print STDERR "temp dir ", $temp_dir, "\n";
	} else {
		$temp_dir = tempdir( CLEANUP => 1 );
	}
	
	chdir $temp_dir;

	# Generate bowtie input
	open(BT_IN, ">tmp_bt.fa");
	my $i = 0;
	foreach my $probe (@{$list}) {
		print BT_IN ">$i\n";
		print BT_IN $probe->raw_sequence."\n";
		$i++;
	}
	close(BT_IN);

	# Run bowtie
	#"/opt/bowtie-0.12.7/bowtie -c -a -X1000 -v3 -p16 --shmem /var/genomes/9606/GRCh37/bowtie/GRCh37 -1 GAGACAGGCCTTTGGATTG -2 CCCACTACCAGACTCAAATG"

	my $bowtie_param;
	if ( ($self->specificity_method eq "thermodynamic") && ($self->index_type eq 'gdna') ) {
		$bowtie_param = "-v2 -a";
	} elsif ( ($self->specificity_method eq "thermodynamic") && ($self->index_type eq 'cdna') ) { 
		$bowtie_param = "-v2 -a --fullref --nofw";
	} elsif ( ($self->specificity_method eq "unique") && ($self->index_type eq 'gdna') ) { 
		$bowtie_param = "-v0 -k2";
	} elsif ( ($self->specificity_method eq "unique") && ($self->index_type eq 'cdna') ) { 
		$bowtie_param = "-v0 -a --fullref --nofw";
	}

	my $bowtie_cmd = sprintf "%s %s -p8 --quiet -r %s -f tmp_bt.fa", $self->bowtie_cmd, $bowtie_param,
							 $self->bowtie_index;
	my $bowtie_pid = open(BT_OUT, $bowtie_cmd." |");


	# Parse bowtie output
	my $mappings = {};
	while (<BT_OUT>) {
		chomp $_;
		my @m = split(/\t/, $_);
		push(@{$mappings->{$m[0]}}, \@m);
	}
	close(BT_OUT);

	# Check mappings
	$i = 0;
	foreach my $probe (@{$list}) {
		my $result = ASODesign::Specificity::Result->new();
		if (my $probe_mappings = $mappings->{$i}) {
			foreach my $mapping (@{$probe_mappings}) {
				my $mm_string = '';
				if (scalar(@{$mapping}) > 7) {
					$mm_string = $mapping->[7];
				}
				my $target = $self->bowtie_mm_seq($probe, $mm_string, $mapping->[1]);

				if ($self->index_type eq 'gdna') {
					$target->chromosome( $mapping->[2] );
				} elsif ($self->index_type eq 'cdna') {
					# skip mappings on scaffolds and haploblocks
					unless( $mapping->[2] =~ /chromosome:\w+:(\w+)/ ) {
						next;
					}
					if (length($1) > 2) {
						next;
					}

					$mapping->[2] =~ /gene:(ENSG\d+)/;
					$target->gene_id( $1 );
					$mapping->[2] =~ /^(ENST\d+)/;
					$target->transcript_id( $1 );
				}
				$target->start( $mapping->[3] );

				my $nr_mismatches = () = $mm_string =~ /\Q,/g;

				if (length($mm_string) eq 0) {
					# Perfect match
					$target->delta_g( $calc->calculate_delta_g($probe, $target) );
					push(@{$result->perfect_matches}, $target);
				} elsif ($nr_mismatches eq 0) {
					# Single mismatch
					$target->delta_g( $calc->calculate_delta_g($probe, $target) );
					push(@{$result->single_mismatches}, $target);
				} elsif ($nr_mismatches eq 1) {
					# Double mismatch
					# cannot calculate DG for consequetive mismatches
					push(@{$result->double_mismatches}, $target);
				}
			}

			$probe->is_specific(0);
			if (($result->nr_perfect_matches <= 1) || ($result->nr_geneids_perfect_matches <= 1)) {
				$probe->start($result->perfect_matches->[0]->start);

				if ($self->index_type eq 'gdna') {
					$probe->chromosome($result->perfect_matches->[0]->chromosome);
				} elsif ($self->index_type eq 'cdna') {
					$probe->gene_id($result->perfect_matches->[0]->gene_id);
					my @transcript_ids =  map {$_->transcript_id} @{$result->perfect_matches};
					$probe->transcript_id( join "|", @transcript_ids );
				}
				

				my $dg_threshold = $result->delta_g_perfect_match() + $self->max_delta_g_diff_offtarget();
				if ($self->specificity_method eq "unique") {
					$probe->is_specific(1);
					$result->is_specific(1);
				} elsif (($self->specificity_method eq "thermodynamic") && ($result->nr_single_mismatches_delta_g($dg_threshold) == 0)) {
					$probe->is_specific(1);
					$result->is_specific(1);
				}
			}
			$probe->specificity($result);
			# #$probe->{nr_of_mappings} = scalar(@{$probe_mappings});
			# if (scalar(@{$probe_mappings}) == 1) {
				
			# 	$probe->chromosome( $probe_mappings->[0]->[2] );
			# 	$probe->start( $probe_mappings->[0]->[3] );
			# 	$probe->is_specific(1);

			# } else {
			# 	#printf("%s %s\n%d %d\n",'chr'.$probe_mappings->[0]->[2], $probe->{chr}, $probe_mappings->[0]->[3]+1, $probe->{pos});
			# 	$probe->is_specific(0);
			# }
		}
		push(@{$result_list}, $result);
		$i++;
	}
	
	waitpid( $bowtie_pid, 0 );

	chdir $curr_dir;
	return $result_list;
}

# return the sequence on the oposite strand of a probe aligned with mismachtes
# based on the sequence of the probe and the bowtie mismatch string
sub bowtie_mm_seq(){
	my $self = shift;
	my $probe = shift;
	my $mm_string = shift;
	my $mm_strand = shift;

	my $complement = $probe->raw_sequence;
	$complement =~ tr/ATGC/TACG/;

	my @mismatches = split(/,/, $mm_string);
	foreach my $mismatch (@mismatches) {
		if ($mismatch =~ /^(\d+):(\w)>(\w)$/) {
			my $probe_base;
			if ($mm_strand eq '+') {
				$complement = $probe->raw_sequence;
				$probe_base = substr($complement, $1, 1, $2);
				$complement =~ tr/ATGC/TACG/;
			} else {
				$probe_base = substr($complement, $1, 1, $2);
			}
			unless ( $probe_base eq $3) {
				confess(sprintf("Not the right base at pos %d, found %s expected %s", $1, $probe_base, $3));
			}
		} else {
			confess("Error parsing mismatches string");
		}
	}

	return ASODesign::Probe->new(sequence => $complement);
}

=head1 AUTHOR

Pieter-Jan Volders, C<< <pieterjan.volders at ugent.be> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-asodesign-probe at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=ASODesign-Probe>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc ASODesign::Specificity::Checker


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=ASODesign-Probe>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/ASODesign-Probe>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/ASODesign-Probe>

=item * Search CPAN

L<http://search.cpan.org/dist/ASODesign-Probe/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2014 Pieter-Jan Volders.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut

1; # End of ASODesign::Specificity::Checker
