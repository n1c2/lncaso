package ASODesign::Accessibility::Calculator;

use Moose; # automatically turns on strict and warnings
use ASODesign::Accessibility::Profile;
use ASODesign::Transcript;
use File::Temp qw(tempdir);
use Cwd;


has 'rnaplfold_cmd' => (is => 'rw', isa => 'Str',  required => 0, default => 'RNAplfold');
has 'rnaplfold_ulength' => (is => 'rw', isa => 'Int',  required => 0, default => 16);
has 'rnaplfold_winsize' => (is => 'rw', isa => 'Int', required => 0, default => 70);

# Get of set the accessibility at a chosen position
# position is 1-based
sub calculate_accessibility_profile {
	my $self = shift;
	my $transcript = shift;

	my $profile = ASODesign::Accessibility::Profile->new(
		ulength => $self->rnaplfold_ulength);

	my $curr_dir = getcwd; # the current directory
	my $temp_dir = tempdir( CLEANUP => 1 );
	
	chdir $temp_dir;
	
	# create the fasta file
	open my $fh, '>', "transcript.fa";
	printf $fh ">%s\n%s", 'target', $transcript->sequence();
	close $fh;

	# construct RNAplfold and execute
	my $run_cmd = sprintf "%s -W %d -L %d -u %d < %s", $self->rnaplfold_cmd, $self->rnaplfold_winsize, $self->rnaplfold_winsize, $self->rnaplfold_ulength, 'transcript.fa';
	`$run_cmd`;

	# parse output _lunp file
	open my $lunp_file, 'target_lunp';
	while (<$lunp_file>) {
		next if $_ =~ /^\s*#/; # skip comment lines
		chomp $_;
		my @row = split(/\t/, $_);

		next if $row[0] < $self->rnaplfold_ulength;
		$profile->accessibility( $row[0] - $self->rnaplfold_ulength + 1, $row[$self->rnaplfold_ulength] );
	}
	close $lunp_file;

	chdir $curr_dir; # change back to the original directory

	$transcript->accessibility_profile($profile);
	return $profile;
}

1; # End of Accessibility::Profile
