package ASODesign::Accessibility::Profile;

use Moose; # automatically turns on strict and warnings
use POSIX;

# array with probability that regions a given length are unpaired for every position
has 'table' => (is => 'rw', isa => 'ArrayRef', default => sub { [] });

has 'ulength' => (is => 'rw', isa => 'Int',  required => 1);

#has 'length' => (is => 'rw', isa => 'Int',  required => 0);

# Get of set the accessibility at a chosen position
# posiyion is 1-based
sub accessibility {
	my $self = shift;
	my $pos = shift;
	$pos--; # convert 1-based to 0-based
	
	# if accessibility is provided => setter
	if ( my $acc = shift ) {
		$self->table->[$pos] = $acc;
	} else {
		return $self->table->[$pos];
	}
}

sub length {
	my $self = shift;
	return scalar @{ $self->table };
}

sub probes_accessibility {
	my $self = shift;
	my $probes = shift;

	my $probe_size = $probes->[0]->length();
	my $nr_of_probes = scalar(@{$probes});

	# add undef if we cannot calculate accessibility for some probes
	if ($self->ulength > $probe_size) {

		for (my $i = 0; $i < floor(($self->ulength - $probe_size)/2); $i++) {
			$probes->[$i]->accessibility(undef);
		}

		for (my $i = 0; $i < ceil(($self->ulength - $probe_size)/2); $i++) {
			$probes->[$nr_of_probes-$i-1]->accessibility(undef);
		}
	}

	my $offset = floor(($self->ulength - $probe_size)/2);

	for (my $i = 1; $i <= $self->length; $i++) {
		# skip first x and last y if the nr of probes if smaller than nr of points measured
		if ($self->ulength < $probe_size) {
			# skip first x
			next if ( $i <= floor(($probe_size-$self->ulength)/2) );
			# skip last y
			next if ( ($self->length - $i) < ceil(($probe_size-$self->ulength)/2) );
		}
		$probes->[$offset+$i-1]->accessibility( $self->accessibility($i) );
	}

	return 1;
}

1; # End of Accessibility::Profile
