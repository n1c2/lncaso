package ASODesign::DimerDeltaG::Calculator;

use 5.006;
use Moose; # automatically turns on strict and warnings
use ASODesign::Probe;
use File::Temp;

has 'unafold_dir' => (isa => 'Str', is => 'rw', required => 1);

sub probes_dimer_delta_g {
	my $self = shift;
	my $probes = shift;

	my $tmpdir = File::Temp->newdir();

	my @sequences;
	foreach my $probe (@{$probes}) {
		push(@sequences, $probe->sequence);
	}

	my $seqfilename = $tmpdir->dirname.'/probes.seq';
	open (SEQFILE, '>'.$seqfilename);
	print SEQFILE join(";", @sequences)."\n";
	close(SEQFILE);
	
	$ENV{UNAFOLDDAT}=$self->unafold_dir.'/data/';
	my $cmd = $self->unafold_dir."/bin/hybrid-min --NA DNA --output=".$tmpdir->dirname."/probes $seqfilename $seqfilename >> /dev/null";
	system($cmd) == 0 or return;

	my $i = 0;
	open(DGFILE, $tmpdir->dirname.'/probes.dG');
	while (my $line = <DGFILE>) {
		next if $line=~ /^\#/;
		chomp $line;
		my @l = split( /\t/, $line );

		$probes->[$i]->dimer_delta_g($l[1]);
		$i++;	
	}
	close(DGFILE);

	return unless ($i == scalar(@{$probes}));

	return 1;

}

sub calculate_dimer_delta_g {
	my $self = shift;
	my $probe = shift;

	$self->probes_dimer_delta_g([$probe]) or return;

	return $probe->dimer_delta_g();
}

1; # End of ASODesign::DimerDeltaG::Calculator
