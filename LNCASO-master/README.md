# ASODesign-Probe

The README is used to introduce the module and provide instructions on
how to install the module, any machine dependencies it may have (for
example C compilers and installed libraries) and any other information
that should be provided before the module is installed.

A README file is required for CPAN modules since CPAN extracts the README
file from a module distribution so that people browsing the archive
can use it to get an idea of the module's uses. It is usually a good idea
to provide version information here so that people can decide whether
fixes for the module are worth downloading.


## INSTALLATION

To install this module, run the following commands:

    perl Makefile.PL
    make
    make test
    make install

Make sure the following enviroment variables are set before running the program:

 - `UNAFOLDDIR`: path to the install location of Unafold 

## DOCKER

Before building the docker image, provide the **Unafold** program in `assets/unafold-3.8`.

Run the following command to download the required reference genomes automatically to the `./genomes` folder.

    ./download_bowtie_genomes.sh /path/to/genomes

Build the image

    docker build -t lncaso:v2 .

Since testing the specificity calculation is not possible at build due to the absence of bowtie index files,
full testing should be performed before running by:

     docker run -v -it /path/to/host/genomes:/genomes --entrypoint bash lncaso:v2
     perl Makefile.PL && make test

To run:

    docker run -v /path/to/host/genomes:/genomes -v /path/to/host/data:/data lncaso:v2 [options] [/data/fasta_file]


## USAGE

Get help with

     design_probes -?


## SUPPORT AND DOCUMENTATION

After installing, you can find documentation for this module with the
perldoc command.

    perldoc ASODesign::Probe

You can also look for information at:

    RT, CPAN's request tracker (report bugs here)
        http://rt.cpan.org/NoAuth/Bugs.html?Dist=ASODesign-Probe

    AnnoCPAN, Annotated CPAN documentation
        http://annocpan.org/dist/ASODesign-Probe

    CPAN Ratings
        http://cpanratings.perl.org/d/ASODesign-Probe

    Search CPAN
        http://search.cpan.org/dist/ASODesign-Probe/


## LICENSE AND COPYRIGHT

Copyright (C) 2014 Pieter-Jan Volders

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.
