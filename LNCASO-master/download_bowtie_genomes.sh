#!/usr/bin/env bash

if [ "$#" -lt 1 ]; then
    echo "Usage: download_bowtie_genomes.sh OUTPUTDIR [CDNA]"
    exit 1
fi

OUTPUTDIR=$1

cd $OUTPUTDIR
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/hg19.ebwt.zip && unzip hg19.ebwt.zip && rm hg19.ebwt.zip
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/mm9.ebwt.zip && unzip mm9.ebwt.zip && rm mm9.ebwt.zip

# Bowtie hg38
wget ftp://ftp.ccb.jhu.edu/pub/data/bowtie_indexes/GRCh38_no_alt.zip && unzip GRCh38_no_alt.zip && rm GRCh38_no_alt.zip
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.1.ebwt hg38.1.ebwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.2.ebwt hg38.2.ebwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.3.ebwt hg38.3.ebwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.4.ebwt hg38.4.ebwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.rev.1.ebwt hg38.rev.1.ebwt
mv GCA_000001405.15_GRCh38_no_alt_analysis_set.rev.2.ebwt hg38.rev.2.ebwt

if [ "$#" -eq 2 ]; then
    wget ftp://ftp.ensembl.org/pub/release-101/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz
    gunzip Homo_sapiens.GRCh38.cdna.all.fa.gz
    wget ftp://ftp.ensembl.org/pub/release-101/fasta/homo_sapiens/ncrna/Homo_sapiens.GRCh38.ncrna.fa.gz
    gunzip Homo_sapiens.GRCh38.ncrna.fa.gz
    cat Homo_sapiens.GRCh38.cdna.all.fa Homo_sapiens.GRCh38.ncrna.fa > hg38_cdna.fa
    rm Homo_sapiens.GRCh38.cdna.all.fa
    rm Homo_sapiens.GRCh38.ncrna.fa
    bowtie-build hg38_cdna.fa hg38_cdna
fi