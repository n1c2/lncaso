# Basic Dockerfile for the ASO design pipeline.
# Before building the docker image, provide the **Unafold** program in `assets/unafold-3.8`.
# 
# Run the following command to download the required reference genomes automatically to the `./genomes` folder.
# 
#     ./download_bowtie_genomes.sh
# 
# Build the image
# 
#     docker build -t lncaso:v2 .
# 
# Since testing is no longer performed at build due to the absence of bowtie index files,
# testing should be performed before running by:
# 
#     docker run -v -it /path/to/host/genomes:/genomes --entrypoint bash lncaso:v2
#     perl Makefile.PL && make test
# 
# To run:
# 
#     docker run -v /path/to/host/genomes:/genomes -v /path/to/host/data:/data lncaso:v2 [options] [/data/fasta_file]

FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y git r-base libmoose-perl libstatistics-r-perl wget libgsl0ldbl bioperl

RUN git clone https://github.ugent.be/vandesompelelab/ASODesign.git

# Vienna RNA
RUN wget https://www.tbi.univie.ac.at/RNA/download/sourcecode/2_1_x/ViennaRNA-2.1.6.tar.gz && tar -zxvf ViennaRNA-2.1.6.tar.gz
WORKDIR /ViennaRNA-2.1.6
RUN ./configure && make && make install

ENV ASODESIGNDNADNALUT /ASODesign/assets/dna_dna_santalucia.txt
ENV ASODESIGNDNADNAMMLUT /ASODesign/assets/dna_dna_mismatch_santalucia.txt
ENV ASODESIGNDNARNALUT /ASODesign/assets/dna_rna_sugimoto.txt
ENV ASODESIGNLNADNALUT /ASODesign/assets/lna_dna_owczarzy.txt
ENV BOWTIEHG19 /genomes/hg19
ENV BOWTIEHG38 /genomes/hg38
ENV BOWTIEHMOUSE /genomes/mm9
ENV BOWTIEHG38CDNA /genomes/hg38_cdna
ENV ASODESIGNGAMMODEL /ASODesign/assets/2014_05_22_final_model.RData
ENV UNAFOLDDIR /usr/local

ADD . /ASODesign
WORKDIR /ASODesign/assets/unafold-3.8/
RUN ./configure && make && make install

WORKDIR /ASODesign
RUN perl Makefile.PL && make && make test && make install

ENTRYPOINT ["design_probes"]