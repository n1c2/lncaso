use 5.006;
use strict;
use warnings;
use Test::More tests => 4;
use ASODesign::Probe;

# 1
use_ok('ASODesign::DimerDeltaG::Calculator');

my $c = ASODesign::DimerDeltaG::Calculator->new(unafold_dir => '/usr/local');

my $p1 = ASODesign::Probe->new(sequence => 'CCGTCCTGGGACAGCC');
my $p2 = ASODesign::Probe->new(sequence => 'GCGTCCGTGAGCTGGG');

# 2
ok( $c->probes_dimer_delta_g([$p1, $p2]), 'Method returns');

# 3
is( $p1->dimer_delta_g, -9.213, 'Calculated DimerDeltaG 1' );

# 4
is( $p2->dimer_delta_g, -4.553, 'Calculated DimerDeltaG é' );
