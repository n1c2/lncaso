use 5.006;
use strict;
use warnings;
use Test::More tests => 1;

ok(1, "I'm to lazy to write decent unit tests for this");

use File::Temp qw(tempdir);
 
my $dir = tempdir( CLEANUP => 1 );
 
print "$dir\n";
 
open my $fh, '>', "$dir/some_file.txt";
print $fh "text";
close $fh;
