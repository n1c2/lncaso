use 5.006;
use strict;
use warnings;
use Test::More tests => 3;
use ASODesign::Probe;
use ASODesign::DeltaG::Calculator;
use ASODesign::DimerDeltaG::Calculator;

# 1
use_ok('ASODesign::Knockdown::Predictor');

my $dg_c     = ASODesign::DeltaG::Calculator->new(lut_file => 'assets/dna_dna_santalucia.txt');
my $dim_dg_c = ASODesign::DimerDeltaG::Calculator->new(unafold_dir => '/usr/local');

my $p1 = ASODesign::Probe->new(sequence => 'CCGTCCTGGGACAGCC');
my $p2 = ASODesign::Probe->new(sequence => 'GCGTCCGTGAGCTGGG');
$p1->accessibility(0.000105762);
$p2->accessibility(0.01282276);

$dg_c->probes_delta_g([$p1, $p2]);
$dim_dg_c->probes_dimer_delta_g([$p1, $p2]);

my $pred = ASODesign::Knockdown::Predictor->new(gam_model => 'assets/2014_05_22_final_model.RData');
$pred->score_probes([$p1, $p2]);

# 2
is( $p1->score, 0.2881543, 'Calculated score 1' );

# 3
is( $p2->score, 0.2807376, 'Calculated score 2' );
