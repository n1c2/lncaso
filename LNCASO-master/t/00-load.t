#!perl -T

use Test::More tests => 3;

BEGIN {
    use_ok( 'ASODesign::Probe' ) || print "Bail out!\n";
    use_ok( 'ASODesign::DeltaG::Calculator' ) || print "Bail out!\n";
    use_ok( 'ASODesign::DeltaG::LookUpTable' ) || print "Bail out!\n";
}

diag( "Testing ASODesign::Probe $ASODesign::Probe::VERSION, Perl $], $^X" );
