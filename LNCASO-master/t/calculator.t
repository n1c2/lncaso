use 5.006;
use strict;
use warnings;
use Test::More tests => 2;
use ASODesign::Probe;

# 1
use_ok('ASODesign::DeltaG::Calculator');

my $c = ASODesign::DeltaG::Calculator->new(lut_file => 'assets/dna_dna_santalucia.txt');

my $p = ASODesign::Probe->new(sequence => 'CGGGCGGATCGCTTGA');

is( $c->calculate_delta_g($p), -22.96, 'Calculate DeltaG' );
