#!perl -T

use 5.006;
use strict;
use warnings;
use Test::More tests => 1;
use ASODesign::Probe;

# 1
use_ok('ASODesign::Modification::PSDNAModification');

my $p = ASODesign::Probe->new(sequence => 'TCGTCAACTGACTGCAT');

my $m = ASODesign::Modification::PSDNAModification->new();