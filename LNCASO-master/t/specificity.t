use 5.006;
use strict;
use warnings;
use Test::More;
use ASODesign::Probe;

plan skip_all => "Bowtie indexes not found" unless -f $ENV{'BOWTIEHG19'}.'.1.ebwt';

use_ok('ASODesign::Specificity::Checker');

my $c = ASODesign::Specificity::Checker->new(
	bowtie_index => $ENV{'BOWTIEHG19'},
	pair_lut_file => 'assets/dna_dna_mismatch_santalucia.txt');

my $p = ASODesign::Probe->new(sequence => 'G*G*G*C*G*C*G*G*C*T*G*A*G*A*T*A');
my $p2 = ASODesign::Probe->new(sequence => 'GGCGTTCATGTGGCGA');

# test the mismatch string conversion
my $p2_as = $c->bowtie_mm_seq($p2, '3:A>G', '+');

is($p2_as->sequence, 'CCGTAAGTACACCGCT', 'bowtie mismatch sense');

my $p2_as_2 = $c->bowtie_mm_seq($p2, '6:A>G', '-');

is($p2_as_2->sequence, 'CCGCAAATACACCGCT', 'bowtie mismatch antisense');


# test the bowtie parsing
my @list = ($p2, $p);

my $results = $c->check_probes(\@list);

is($results->[0]->nr_perfect_matches, 1, 'nr perfect matches');
is($results->[0]->nr_single_mismatches, 5, 'nr single mismatches');
is($results->[0]->nr_double_mismatches, 159, 'nr double mismatches');

is($p2->chromosome, 'chr12', "Chromosome");
is($p2->start, 54357601, "Start position");

# test specificity calculation
is($results->[0]->delta_g_perfect_match, -22.2, 'delta g perfect match');
is($p2->specificity->delta_g_perfect_match, -22.2, 'delta g perfect match from probe');

my $dg_threshold = $results->[0]->delta_g_perfect_match + 3;
is($results->[0]->nr_single_mismatches_delta_g($dg_threshold), 0, 
	'nr single mismatches delta g > 3');

$dg_threshold = $results->[0]->delta_g_perfect_match + 4;
is($results->[0]->nr_single_mismatches_delta_g($dg_threshold), 1, 
	'nr single mismatches delta g > 4');

ok( $list[0]->is_specific, "Is specific");

$dg_threshold = $results->[1]->delta_g_perfect_match + 3;
is($results->[1]->nr_single_mismatches_delta_g($dg_threshold), 3, 
	'nr single mismatches delta g > 3');

$dg_threshold = $results->[1]->delta_g_perfect_match + 4;
is($results->[1]->nr_single_mismatches_delta_g($dg_threshold), 6, 
	'nr single mismatches delta g > 4');

ok( !$list[1]->is_specific, "Is not specific");

# check the UNIQUE specificity calculation

$c->specificity_method('unique');
my @list_unique = ($p);
my $results_unique = $c->check_probes(\@list_unique);


# $dg_threshold = $results_unique->[0]->delta_g_perfect_match + 3;
# is($results_unique->[0]->nr_single_mismatches_delta_g($dg_threshold), 3, 
# 	'nr single mismatches delta g > 3');

is($list_unique[0]->sequence, 'G*G*G*C*G*C*G*G*C*T*G*A*G*A*T*A');
ok($list_unique[0]->is_specific, "Is specific eventhough 3 mismatches delta g > 3");



# # HG38
note("HG38");

my $c38 = ASODesign::Specificity::Checker->new(
	bowtie_index => $ENV{'BOWTIEHG38'},
	pair_lut_file => 'assets/dna_dna_mismatch_santalucia.txt');
$c38->check_probes(\@list);
is($p2->start, 53963817, "Start position");

# cDNA specificity
note("cDNA specificity");

my $c_cdna = ASODesign::Specificity::Checker->new(
	bowtie_index => $ENV{'BOWTIEHG38CDNA'},
	specificity_method => 'unique',
	index_type => 'cdna',
	pair_lut_file => 'assets/dna_dna_mismatch_santalucia.txt');

my $p_hotair = ASODesign::Probe->new(sequence => 'GGCGTTCATGTGGCGA');
my $p_PRPF31 = ASODesign::Probe->new(sequence => 'CCGCTGCAGCGTCTTG');

my $results_cdna =$c_cdna->check_probes([$p_hotair, $p_PRPF31]);

is($results_cdna->[0]->perfect_matches->[0]->gene_id, 'ENSG00000228630', "First hit matches ENG ID");
is($p_hotair->gene_id, 'ENSG00000228630', "Matches ENSG ID");
is($p_hotair->transcript_id, 'ENST00000425595|ENST00000455246|ENST00000424518', "Matches ENST IDs");

ok($p_hotair->is_specific, "Is specific");

ok(!$p_PRPF31->is_specific, "PRPF31 is not specific");
is($results_cdna->[1]->nr_perfect_matches, 7, "7 perfect matches for PRPF31 ASO");
is($results_cdna->[1]->nr_single_mismatches, 0, "0 single mismatches for PRPF31 ASO 'unique' method");

# cDNA thermo
my $c_cdna_thermo = ASODesign::Specificity::Checker->new(
	bowtie_index => $ENV{'BOWTIEHG38CDNA'},
	index_type => 'cdna',
	pair_lut_file => 'assets/dna_dna_mismatch_santalucia.txt');
my $results_cdna_thermo =$c_cdna_thermo->check_probes([$p_hotair, $p_PRPF31]);
is($results_cdna_thermo->[1]->nr_single_mismatches, 10, "10 single mismatches for PRPF31 ASO 'thermodynamic' method");

done_testing();


# is( $c->calculate_delta_g($p), -22.96, 'Calculate DeltaG' );
