#!perl -T

use 5.006;
use strict;
use warnings;
use Test::More;

use_ok('ASODesign::DeltaG::PairLookUpTable::NearestNeighbors');

use_ok('ASODesign::DeltaG::PairLookUpTable');

ok(my $lut = ASODesign::DeltaG::PairLookUpTable->new(filename => 'assets/dna_dna_mismatch_santalucia.txt'));

is($lut->nn('TC', 'AG')->delta_g, -1.3, 'File parsed correctly');
is($lut->nn('GT', 'CC')->delta_g, 0.98, 'File parsed correctly');

done_testing();

# my $lut = ASODesign::DeltaG::PairLookUpTable->new(filename => 'assets/dna_dna_santalucia.txt', auto_construct => 0);

# # 4
# ok(!$lut->auto_construct, 'auto_construct setter');

# # 5
# ok($lut->add_nn($nn), 'Add NearestNeighbors to PairLookUpTable');

# # 6
# is($lut->nn('NN')->delta_g, -2.3, 'Get NearestNeighbors from PairLookUpTable');

# # 7
# ok(!$lut->add_nn($nn), 'Cannot add existing NearestNeighbors to PairLookUpTable');

# $lut->construct();

# # 8
# # TA	-0.59
# is($lut->nn('TA')->delta_g, -0.59, 'File parsed correctly');
