#!perl -T

use 5.006;
use strict;
use warnings;
use Test::More tests => 8;

# 1
use_ok('ASODesign::DeltaG::LookUpTable::NearestNeighbors');

my $nn = ASODesign::DeltaG::LookUpTable::NearestNeighbors->new(sequence=>'NN', delta_g=>-2.3);

# 2
is  ( $nn->delta_g, -2.3, 'NearestNeighbors delta_g getter' );

# 3
use_ok('ASODesign::DeltaG::LookUpTable');

my $lut = ASODesign::DeltaG::LookUpTable->new(filename => 'assets/dna_dna_santalucia.txt', auto_construct => 0);

# 4
ok(!$lut->auto_construct, 'auto_construct setter');

# 5
ok($lut->add_nn($nn), 'Add NearestNeighbors to LookUpTable');

# 6
is($lut->nn('NN')->delta_g, -2.3, 'Get NearestNeighbors from LookUpTable');

# 7
ok(!$lut->add_nn($nn), 'Cannot add existing NearestNeighbors to LookUpTable');

$lut->construct();

# 8
# TA	-0.59
is($lut->nn('TA')->delta_g, -0.59, 'File parsed correctly');