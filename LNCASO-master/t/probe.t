#!perl -T

use 5.006;
use strict;
use warnings;
use Test::More tests => 8;

# 1
use_ok('ASODesign::Probe');

my $p = ASODesign::Probe->new(sequence => 'A*C*G*T*A*C*G*T*A*C*G*T*A*C*G*T*A*C*T*T');

my $p2 = ASODesign::Probe->new(sequence => '+A*+C*+T*C*G*T*A*C*G*T*A*C*T*+G*+T*+A');

# 2
is ($p->length, 20, 'Calculate length of PS-DNA ASO');

# 3
is ($p2->length, 16, 'Calculate length of LNA Gapmer');

my @terminal_bases = $p->terminal_bases;
my @expected_terminal_bases = ('A', 'T');

# 4
is_deeply (\@terminal_bases, \@expected_terminal_bases, 'Terminal bases of PS-DNA ASO');

@terminal_bases = $p2->terminal_bases;
@expected_terminal_bases = ('+A', '+A');

# 5
is_deeply (\@terminal_bases, \@expected_terminal_bases, 'Terminal bases of LNA Gapmer');

my @neighbors = $p->neighbors;
my @expected_neighbors = ('AC', 'CG', 'GT', 'TA', 'AC', 'CG', 'GT', 'TA', 'AC', 'CG', 'GT', 'TA', 'AC', 'CG', 'GT', 'TA', 'AC', 'CT', 'TT');

# 6
is_deeply (\@neighbors, \@expected_neighbors, 'Neighbors of PS-DNA ASO');

@neighbors = $p2->neighbors;
@expected_neighbors = ('+A+C', '+C+T', '+TC', 'CG', 'GT', 'TA', 'AC', 'CG', 'GT', 'TA', 'AC', 'CT', 'T+G', '+G+T', '+T+A');

# 7
is_deeply (\@neighbors, \@expected_neighbors, 'Neighbors of LNA Gapmer');

# 8
is($p2->raw_sequence, 'ACTCGTACGTACTGTA', 'Raw sequence of probe');
