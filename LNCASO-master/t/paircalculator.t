use 5.006;
use strict;
use warnings;
use Test::More;
use ASODesign::Probe;

# 1
use_ok('ASODesign::DeltaG::PairCalculator');

ok (my $c = ASODesign::DeltaG::PairCalculator->new(lut_file => 'assets/dna_dna_mismatch_santalucia.txt'));

# Perfect match
my $p = ASODesign::Probe->new(sequence => 'CGTTGA');
my $m = ASODesign::Probe->new(sequence => 'GCAACT');

is( $c->calculate_delta_g($p, $m), -5.35, 'Perfect match DeltaG' );

# Internal mismatch
my $p2 = ASODesign::Probe->new(sequence => 'GGACTGACG');
my $m2 = ASODesign::Probe->new(sequence => 'CCTGGCTGC');
my $m3 = ASODesign::Probe->new(sequence => 'CCTGACTGC');

is( $c->calculate_delta_g($p2, $m2), -8.32, 'Internal mismatch DeltaG' );

cmp_ok( $c->calculate_delta_g($p2, $m3), '<', $c->calculate_delta_g($p2, $m2), 'Perfect match lower DeltaG' );

my @probes = (
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCCCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGTCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCCCGCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCTCGCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCACGCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCACGCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCCCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCCCCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGTCGACTCTAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGCCGACTCTAA')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGCCGACTCTTT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGCCGACTCAAT')],
	[ASODesign::Probe->new(sequence => 'GGGCGCGGCTGAGATA'),	ASODesign::Probe->new(sequence => 'CCCGCGCCGATTCTAT')] );

is($c->calculate_delta_g($probes[0]->[0], $probes[0]->[1]),   -22.64, "DeltaG correct");
is($c->calculate_delta_g($probes[1]->[0], $probes[1]->[1]),   -16.74, "DeltaG correct");
is($c->calculate_delta_g($probes[2]->[0], $probes[2]->[1]),   -19.42, "DeltaG correct");
is($c->calculate_delta_g($probes[3]->[0], $probes[3]->[1]),   -16.74, "DeltaG correct");
is($c->calculate_delta_g($probes[4]->[0], $probes[4]->[1]),   -17.21, "DeltaG correct");
is($c->calculate_delta_g($probes[5]->[0], $probes[5]->[1]),   -17.01, "DeltaG correct");
is($c->calculate_delta_g($probes[6]->[0], $probes[6]->[1]),   -17.01, "DeltaG correct");
is($c->calculate_delta_g($probes[7]->[0], $probes[7]->[1]),   -16.74, "DeltaG correct");
is($c->calculate_delta_g($probes[8]->[0], $probes[8]->[1]),   -16.74, "DeltaG correct");
is($c->calculate_delta_g($probes[9]->[0], $probes[9]->[1]),   -19.42, "DeltaG correct");
is($c->calculate_delta_g($probes[10]->[0], $probes[10]->[1]), -21.37, "DeltaG correct");
is($c->calculate_delta_g($probes[11]->[0], $probes[11]->[1]), -19.81, "DeltaG correct");
is($c->calculate_delta_g($probes[12]->[0], $probes[12]->[1]), -19.68, "DeltaG correct");
is($c->calculate_delta_g($probes[13]->[0], $probes[13]->[1]), -19.12, "DeltaG correct");

my $my_probe_2 = ASODesign::Probe->new(sequence => 'GGCGTTCATGTGGCGA');
my @probes2 = (
	ASODesign::Probe->new(sequence => 'CCGCAAGTACACCGCT'),
	ASODesign::Probe->new(sequence => 'CCGCAAGTACACCTCT'),
	ASODesign::Probe->new(sequence => 'CCGTAAGTACACCGCT'),
	ASODesign::Probe->new(sequence => 'CCGCAATTACACCGCT'),
	ASODesign::Probe->new(sequence => 'CCGCAAATACACCGCT'),
	ASODesign::Probe->new(sequence => 'CCGCAAGTACACCCCT') );

# foreach my $t (@probes2) {
# 	diag($c->calculate_delta_g($my_probe_2, $t));
# }

done_testing();